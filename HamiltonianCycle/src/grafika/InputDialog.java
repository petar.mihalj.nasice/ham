package grafika;

import java.awt.Dimension;
import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class InputDialog extends JDialog{
	
	private JTextArea jta;
	
	public InputDialog(Frame owner) {
		super(owner);
		
		this.setMinimumSize(new Dimension(400,400));
		jta = new JTextArea("Input DIMACS instead of this text and click X to continue.");
		jta.setVisible(true);
		this.getContentPane().add(new JScrollPane(jta));
	}
	
	public String getInput() {
		return jta.getText();
	}
}
