package grafika;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import graphs.EdgeType;
import graphs.VertexType;

public class Prikaz extends JPanel{
	private static final long serialVersionUID = 1L;
	
	public Prikaz() {

	}
	
	@Override
	protected void paintComponent(Graphics g) {
		if (RuntimeContainer.onConfig) {
			if (RuntimeContainer.graph==null) {
				
			}
			else {
				drawConfig(g);
			}
		}
		else {
			drawRuntime(g);
		}
		
	}	
	
	public void drawRuntime(Graphics g) {	
		EdgeType[][] adjMatrix=RuntimeContainer.method.getGraph();
		VertexType[] vertices=RuntimeContainer.method.getVertices();
		int n=adjMatrix.length;
		
		int[][] c = new int[n][2];
		double angle=0;
		for (int i=0;i<n;i++){
			c[i][0]=(int) (Constants.centerX+Math.cos(angle)*Constants.len);
			c[i][1]=(int) (Constants.centerY+Math.sin(angle)*Constants.len);
			angle+=2*Math.PI/n;
		}
		
		for (int i=0;i<n;i++){
			for (int j=i+1;j<n;j++){
				if (adjMatrix[i][j]==EdgeType.EXISTS_NOT_TAKEN){
					Graphics2D g2 = (Graphics2D) g;
				    g2.setStroke(new BasicStroke(3));
					g.setColor(Color.BLACK);
					g.drawLine(c[i][0]+Constants.size/2, c[i][1]+Constants.size/2, c[j][0]+Constants.size/2, c[j][1]+Constants.size/2);
					g2.setStroke(new BasicStroke(1));
				}
				else if (adjMatrix[i][j]==EdgeType.EXISTS_TAKEN){
					Graphics2D g2 = (Graphics2D) g;
				    g2.setStroke(new BasicStroke(3));
					g.setColor(Color.RED);
					g.drawLine(c[i][0]+Constants.size/2, c[i][1]+Constants.size/2, c[j][0]+Constants.size/2, c[j][1]+Constants.size/2);
					g2.setStroke(new BasicStroke(1));
				}
				else if (adjMatrix[i][j]==EdgeType.EXISTS_IGNORE){
					g.setColor(Color.GRAY);
					g.drawLine(c[i][0]+Constants.size/2, c[i][1]+Constants.size/2, c[j][0]+Constants.size/2, c[j][1]+Constants.size/2);
				}
			}
		}
		
		for (int i=0;i<n;i++){
			if (vertices[i]==VertexType.TAKEN){
				g.setColor(Color.RED);
			}
			else if (vertices[i]==VertexType.TAKEN_IGNORE){
				g.setColor(Color.GRAY);
			}
			else{
				g.setColor(Color.WHITE);
			}
			
			g.fillOval(c[i][0], c[i][1], Constants.size, Constants.size);
			g.setColor(Color.BLACK);
			g.drawString(Integer.toString(i), c[i][0]+Constants.size/2, c[i][1]+Constants.size/2);
			g.drawOval(c[i][0], c[i][1], Constants.size, Constants.size);
		}
	}
	
	public void drawConfig(Graphics g) {	
		int[][] adjMatrix=RuntimeContainer.graph;
		int n=adjMatrix.length;
		
		int[][] c = new int[n][2];
		double angle=0;
		for (int i=0;i<n;i++){
			c[i][0]=(int) (Constants.centerX+Math.cos(angle)*Constants.len);
			c[i][1]=(int) (Constants.centerY+Math.sin(angle)*Constants.len);
			angle+=2*Math.PI/n;
		}
		
		for (int i=0;i<n;i++){
			for (int j=i+1;j<n;j++){
				if (adjMatrix[i][j]==1){
					Graphics2D g2 = (Graphics2D) g;
				    g2.setStroke(new BasicStroke(3));
					g.setColor(Color.BLACK);
					g.drawLine(c[i][0]+Constants.size/2, c[i][1]+Constants.size/2, c[j][0]+Constants.size/2, c[j][1]+Constants.size/2);
					g2.setStroke(new BasicStroke(1));
				}
			}
		}
		
		for (int i=0;i<n;i++){	
			g.setColor(Color.WHITE);
			g.fillOval(c[i][0], c[i][1], Constants.size, Constants.size);
			g.setColor(Color.BLACK);
			g.drawString(Integer.toString(i), c[i][0]+Constants.size/2, c[i][1]+Constants.size/2);
			g.drawOval(c[i][0], c[i][1], Constants.size, Constants.size);
		}
	}
}
