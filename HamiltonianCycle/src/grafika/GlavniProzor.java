package grafika;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JSplitPane;
import javax.swing.WindowConstants;

public class GlavniProzor extends JFrame{
	private static final long serialVersionUID = 1L;

	public RuntimePanel runtimePanel;
	public ConfigurationPanel configPanel;
	public JSplitPane splitPane;

	public GlavniProzor() {
		RuntimeContainer.gp=this;
        setSize(1800,1000);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.getContentPane().setLayout(new BorderLayout());  
        
        runtimePanel = new RuntimePanel();
        runtimePanel.setVisible(true);
        
        configPanel = new ConfigurationPanel();
        configPanel.setVisible(true);
        
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setDividerLocation(1000);
        splitPane.setLeftComponent(runtimePanel);
        splitPane.setRightComponent(configPanel);
        
        runtimePanel.rt.goGenerate();
        
        this.getContentPane().add(splitPane, BorderLayout.CENTER);
        splitPane.setVisible(true);
    }
}