package grafika;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ConfigurationPanel  extends JPanel{
	private static final long serialVersionUID = 4195284508777744411L;
	public ConfigurationToolbar ct;
	public JTextArea text;
	
	public ConfigurationPanel() {
		this.setLayout(new BorderLayout());
		
        ct=new ConfigurationToolbar();
        ct.setVisible(true);
        this.add(ct,BorderLayout.NORTH);
        
        text=new JTextArea();
        text.setEditable(false);
        text.setVisible(true);
        text.setText("Welcome\n");
        
        this.add(new JScrollPane(text),BorderLayout.CENTER);
	}
}
