package grafika;

import java.awt.Dimension;
import java.awt.Frame;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class OutputDialog extends JDialog{
	
	private JTextArea jta;
	
	public OutputDialog(Frame owner, String dimacs) {
		super(owner);
		this.setMinimumSize(new Dimension(400,400));
		jta = new JTextArea("c Your DIMACS graph is generated here\n"+dimacs);
		jta.setVisible(true);
		jta.setEditable(false);
		this.getContentPane().add(new JScrollPane(jta));
	}
}
