package grafika;

import java.awt.Dialog.ModalityType;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import graphs.GraphFactory;
import methods.Bruteforce;
import methods.BruteforceCHVATAL;
import methods.BruteforceORE;

public class ConfigurationToolbar extends JPanel {
	private static final long serialVersionUID = -1405510180524390994L;
	public JButton newRandomGraphButton;
	public JButton loadGraphFromClipboard;
	public JTextField probabilityTextField;
	public JTextField verticesTextField;
	public JComboBox<String> methodComboBox;
	public JButton execute;
	public JButton sprint;
	
	public ConfigurationToolbar() {
		this.setLayout(new FlowLayout());
		
        loadGraphFromClipboard = new JButton("LoadFromClipboard");
        loadGraphFromClipboard.addActionListener((ActionEvent e)->{
        	try {
        		InputDialog id=new InputDialog(RuntimeContainer.gp);
        		id.setModalityType(ModalityType.APPLICATION_MODAL);
        		id.setVisible(true);
				int[][] graph = GraphFactory.loadFromDimacs(id.getInput());
				if (graph==null) {
					RuntimeContainer.gp.configPanel.text.append("Wrong DIMACS\n");
				}
				else {
					RuntimeContainer.gp.configPanel.text.append("DIMACS graph loaded\n");
					RuntimeContainer.graph=graph;
					RuntimeContainer.gp.runtimePanel.repaint();
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
        });
        this.add(loadGraphFromClipboard);
        
        
        newRandomGraphButton = new JButton("NewGraph");
        newRandomGraphButton.addActionListener((ActionEvent e)->{
        	RuntimeContainer.graph=GraphFactory.randomSimpleGraph(
            			Integer.parseInt(verticesTextField.getText()), 
            			Double.parseDouble(probabilityTextField.getText())
        				);
        	RuntimeContainer.gp.repaint();
        }); 
        this.add(newRandomGraphButton);
        
        this.add(new JLabel("Vjerojatnost:"));
        probabilityTextField = new JTextField("0.7");
        this.add(probabilityTextField);
        
        this.add(new JLabel("Broj cvorova:"));
        verticesTextField = new JTextField("10");
        this.add(verticesTextField);
        
        String[] choices = { "Bruteforce","BruteforceORE","BruteforceCHVATAL"};
        methodComboBox= new JComboBox<String>(choices);
        this.add(methodComboBox);
        
        execute = new JButton("Execute");
        this.add(execute);
        execute.addActionListener((ActionEvent e)->{
        	if (RuntimeContainer.graph!=null) {
        		if (methodComboBox.getSelectedIndex()==0) {
            		RuntimeContainer.method=new Bruteforce(GraphFactory.copy(RuntimeContainer.graph));
            	}
            	else if (methodComboBox.getSelectedIndex()==1) {
            		RuntimeContainer.method=new BruteforceORE(GraphFactory.copy(RuntimeContainer.graph));
            	}
            	else if (methodComboBox.getSelectedIndex()==2) {
            		RuntimeContainer.method=new BruteforceCHVATAL(GraphFactory.copy(RuntimeContainer.graph));
            	}
            	this.getParent().repaint();
            	goExecute();
        	}
        	else {
        		RuntimeContainer.gp.configPanel.text.append("Graph not generated\n");
        	}
        }); 
        
        sprint = new JButton("Sprint");
        this.add(sprint);
        sprint.addActionListener((ActionEvent e)->{
        	if (RuntimeContainer.graph!=null) {
        		if (methodComboBox.getSelectedIndex()==0) {
            		RuntimeContainer.method=new Bruteforce(GraphFactory.copy(RuntimeContainer.graph));
            	}
            	else if (methodComboBox.getSelectedIndex()==1) {
            		RuntimeContainer.method=new BruteforceORE(GraphFactory.copy(RuntimeContainer.graph));
            	}
            	else if (methodComboBox.getSelectedIndex()==2) {
            		RuntimeContainer.method=new BruteforceCHVATAL(GraphFactory.copy(RuntimeContainer.graph));
            	}
        		
        		RuntimeContainer.sprintFinished=false;
        		LocalDateTime start=LocalDateTime.now();
        		while (RuntimeContainer.sprintFinished==false ) {
        			RuntimeContainer.method.nextStep();
        			LocalDateTime endA=LocalDateTime.now();
        			long sSec=start.toEpochSecond(ZoneOffset.ofHours(0));
            		long eSec=endA.toEpochSecond(ZoneOffset.ofHours(0));
            		if (eSec-sSec>10) break;
        		}
        		LocalDateTime end=LocalDateTime.now();
        		
        		if (RuntimeContainer.sprintFinished) {
            		RuntimeContainer.gp.configPanel.text.append("Sprint finished!\n");
            		RuntimeContainer.gp.configPanel.text.append("RESULT: "
            					+(RuntimeContainer.sprintFound?"Hamilton cycle found.":"Hamilton cycle not found.")+"\n");
            		
            		long sSec=start.toEpochSecond(ZoneOffset.ofHours(0));
            		long eSec=end.toEpochSecond(ZoneOffset.ofHours(0));
            		
            		long sNsec=sSec*1000000000+start.getNano();
            		long eNsec=eSec*1000000000+end.getNano();
            		
            		RuntimeContainer.gp.configPanel.text.append("TIME: "+(eNsec-sNsec)/1000000+" miliseconds.\n");
        		}
        		else {
        			RuntimeContainer.gp.configPanel.text.append("Sprint timed out (10 seconds)!\n");
        		}
        		
        		RuntimeContainer.sprintFinished=false;
        		 
            	RuntimeContainer.gp.repaint();
        	}
        	else {
        		RuntimeContainer.gp.configPanel.text.append("Graph not generated\n");
        	}
        });   
	}
	
	public void goExecute() {
		this.execute.setEnabled(false);
		this.methodComboBox.setEnabled(false);
		this.newRandomGraphButton.setEnabled(false);	
		this.loadGraphFromClipboard.setEnabled(false);	
		this.probabilityTextField.setEnabled(false);
		this.verticesTextField.setEnabled(false);
		
		RuntimeContainer.gp.runtimePanel.rt.next100000StepsButton.setEnabled(true);
		RuntimeContainer.gp.runtimePanel.rt.next100StepsButton.setEnabled(true);
		RuntimeContainer.gp.runtimePanel.rt.nextStepButton.setEnabled(true);
		RuntimeContainer.gp.runtimePanel.rt.terminate.setEnabled(true);
		
		RuntimeContainer.onConfig=false;
		RuntimeContainer.gp.repaint();
	}
}
