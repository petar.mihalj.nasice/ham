package grafika;

import java.awt.BorderLayout;

import javax.swing.JPanel;

public class RuntimePanel  extends JPanel{
	private static final long serialVersionUID = 3974125072372698870L;
	public RuntimeToolbar rt;
	public Prikaz prikaz;
	
	public RuntimePanel() {
		this.setLayout(new BorderLayout());
		
        rt=new RuntimeToolbar();
        rt.setVisible(true);
        this.add(rt,BorderLayout.NORTH);
        
        prikaz=new Prikaz();
        prikaz.setVisible(true);
        this.add(prikaz,BorderLayout.CENTER);
	}
}
