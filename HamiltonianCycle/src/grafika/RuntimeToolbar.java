package grafika;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JPanel;

public class RuntimeToolbar extends JPanel {
	private static final long serialVersionUID = -1405510180524390994L;
	public JButton nextStepButton;
	public JButton next100StepsButton;
	public JButton next100000StepsButton;
	public JButton terminate;
	
	private void execSteps(int no) {
		for (int i=0;i<no;i++) RuntimeContainer.method.nextStep();
    	for (String s:RuntimeContainer.method.msg) {
        	RuntimeContainer.gp.configPanel.text.append(s);
    	}
    	RuntimeContainer.method.msg.clear();
    	RuntimeContainer.gp.repaint();
	}
	
	public RuntimeToolbar() {
		this.setLayout(new FlowLayout());
		
		nextStepButton = new JButton("NextStep");
		nextStepButton.addActionListener((ActionEvent e)->{
			execSteps(1);
			RuntimeContainer.gp.configPanel.text.append("Executed 1 step\n");
        }); 
        this.add(nextStepButton);
        
        next100StepsButton = new JButton("Next100Steps");
        next100StepsButton.addActionListener((ActionEvent e)->{
        	execSteps(100);
        	RuntimeContainer.gp.configPanel.text.append("Executed 100 steps\n");
        }); 
        this.add(next100StepsButton);
        
        next100000StepsButton = new JButton("Next100000Steps");
        next100000StepsButton.addActionListener((ActionEvent e)->{
        	execSteps(100000);
        	RuntimeContainer.gp.configPanel.text.append("Executed 100000 steps\n");
        }); 
        this.add(next100000StepsButton);
        
        terminate=new JButton("Terminate");
        this.add(terminate);
        terminate.addActionListener((ActionEvent e)->{
        	RuntimeContainer.method=null;
        	goGenerate();
        	this.getParent().repaint();
        }); 
	}
	
	public void goGenerate() {
		RuntimeContainer.gp.configPanel.ct.loadGraphFromClipboard.setEnabled(true);
		RuntimeContainer.gp.configPanel.ct.methodComboBox.setEnabled(true);	
		RuntimeContainer.gp.configPanel.ct.newRandomGraphButton.setEnabled(true);	
		RuntimeContainer.gp.configPanel.ct.probabilityTextField.setEnabled(true);
		RuntimeContainer.gp.configPanel.ct.verticesTextField.setEnabled(true);
		RuntimeContainer.gp.configPanel.ct.execute.setEnabled(true);
		
		this.next100000StepsButton.setEnabled(false);
		this.next100StepsButton.setEnabled(false);
		this.nextStepButton.setEnabled(false);
		this.terminate.setEnabled(false);
		
		RuntimeContainer.onConfig=true;
		RuntimeContainer.gp.repaint();
	}
}
