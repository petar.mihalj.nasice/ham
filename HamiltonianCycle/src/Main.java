import javax.swing.SwingUtilities;

import grafika.GlavniProzor;
import grafika.RuntimeContainer;
import graphs.GraphFactory;
import methods.Bruteforce;


public class Main {
	public static void main(String[] args) {
		
		SwingUtilities.invokeLater(()->{
			GlavniProzor gp = new GlavniProzor();
			gp.setVisible(true);
		});
		
	}
}
