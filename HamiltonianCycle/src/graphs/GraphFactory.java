package graphs;
import java.util.Random;

public class GraphFactory {
	public static int[][] randomSimpleGraph(int nodes, double p){
		int[][] adjMatrix = new int[nodes][nodes];
		
		Random rng=new Random();
		
		for (int i=0;i<nodes;i++){
			for (int j=0;j<nodes;j++){
				adjMatrix[i][j]=0;
			}
		}
		
		for (int i=0;i<nodes;i++){
			for (int j=i+1;j<nodes;j++){
				if (rng.nextDouble()<=p){
					adjMatrix[i][j]=1;
					adjMatrix[j][i]=1;
				}
			}
		}
		
		return adjMatrix;
	}
	
	public static int[][] loadFromDimacs(String dimacs){
		String[] lines=dimacs.split("\n");
		int n;
		int[][] ret=null;
		
		for (String line:lines) {
			if (line.charAt(0)=='c') {
				continue;
			}
			else if (line.charAt(0)=='p') {
				String[] spl=line.split(" ");
				n=Integer.parseInt(spl[2]);
				ret = new int[n][n];
				
			}
			else if (line.charAt(0)=='e') {
				String[] spl=line.split(" ");
				int u=Integer.parseInt(spl[1])-1;
				int v=Integer.parseInt(spl[2])-1;
				ret[u][v]=1;
				ret[v][u]=1;
			}
		}
		return ret;
	}
	
	public static int[][] copy(int[][] graph){
		int [][] res = new int[graph.length][];
		for (int i = 0; i < graph.length; i++) {
			res[i] = graph[i].clone();
		}
		return res;
	}
}
