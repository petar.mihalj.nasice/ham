package graphs;

public enum VertexType {
	NOT_TAKEN, TAKEN, TAKEN_IGNORE
}