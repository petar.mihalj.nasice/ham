package methods;

import graphs.EdgeType;
import graphs.VertexType;

import java.util.ArrayList;
import java.util.Stack;

import grafika.RuntimeContainer;

public class BruteforceORE extends Method {
	
	static class VertexRestore{
		int i;
		VertexType restoreType;
		public int getI() {
			return i;
		}
		public VertexType getRestoreType() {
			return restoreType;
		}
		public VertexRestore(int i, VertexType restoreType) {
			super();
			this.i = i;
			this.restoreType = restoreType;
		}
	}
	
	static class EdgeRestore{
		int i,j;
		EdgeType restoreType;
		public int getI() {
			return i;
		}
		public EdgeType getRestoreType() {
			return restoreType;
		}
		public EdgeRestore(int i, int j, EdgeType restoreType) {
			super();
			this.i = i;
			this.j = j;
			this.restoreType = restoreType;
		}
	}
	
	static class StackFrame{
		int currentEdge;
		int currentVertex;
		int originVertex;
		int len;
		boolean decidedWithOre;
		private ArrayList<EdgeRestore> er;
		
		public void restore(EdgeType[][] adjMatrix) {
			for (EdgeRestore r:er) {
				adjMatrix[r.i][r.j]=r.restoreType;
			}
		}
		
		public void addRestore(int i, int j, EdgeType restoreType) {
			er.add(new EdgeRestore(i, j, restoreType));
		}
		
		public StackFrame(int currentVertex, int originVertex, int currentEdge, int len, boolean decidedWithOre) {
			super();
			this.currentEdge = currentEdge;
			this.originVertex=originVertex;
			this.currentVertex = currentVertex;
			this.len=len;
			this.er=new ArrayList<>();
			this.decidedWithOre=decidedWithOre;
		}
	}
	
	private Stack<StackFrame> searchStack = new Stack<>();
	private EdgeType[][] adjMatrix;
	private VertexType[] vertices;
	private int n;
	
	public BruteforceORE(int[][] adjMatrix) {
		this.n=adjMatrix.length;
		this.adjMatrix=new EdgeType[n][n];
		this.vertices=new VertexType[n];
		for (int i=0;i<n;i++) {
			vertices[i]=VertexType.NOT_TAKEN;
			for (int j=0;j<n;j++) {
				this.adjMatrix[i][j]=adjMatrix[i][j]==1?EdgeType.EXISTS_NOT_TAKEN:EdgeType.DOESNT_EXIST;
			}
		}
		vertices[0]=VertexType.TAKEN;
		searchStack.add(new StackFrame(0, -1, -1, 0,false));
	}
	
	public void nextStep() {
		if (searchStack.empty()) {
			RuntimeContainer.sprintFinished=true;
			RuntimeContainer.sprintFound=false;
			return;
		}
		StackFrame f = searchStack.peek();
		if (f.len==n) {
			RuntimeContainer.sprintFinished=true;
			RuntimeContainer.sprintFound=true;
			return;
		}
		
		if (f.currentEdge==-1 && f.len>=1 && f.len<n-2) {
			int res=tryAndDoOre(f.currentVertex, f.originVertex);
			if (res!=-1) {
				searchStack.push(new StackFrame(res,f.currentVertex, -1,f.len+1,true));
				msg.add("Decided with ORE\n");
				return;
			}
		}
		
		f.currentEdge++;
		if (f.len==n-1) {
			if (adjMatrix[f.currentVertex][0]==EdgeType.EXISTS_NOT_TAKEN) f.currentEdge=0;
			else {
				searchStack.pop();
				adjMatrix[searchStack.peek().currentVertex][searchStack.peek().currentEdge]=EdgeType.EXISTS_NOT_TAKEN;
				adjMatrix[searchStack.peek().currentEdge][searchStack.peek().currentVertex]=EdgeType.EXISTS_NOT_TAKEN;
				vertices[searchStack.peek().currentEdge]=VertexType.NOT_TAKEN;
				vertices[searchStack.peek().currentVertex]=VertexType.TAKEN;
				searchStack.peek().restore(adjMatrix);
				return;
			}
		}
		else {
			while (f.currentEdge<n && (adjMatrix[f.currentVertex][f.currentEdge]!=EdgeType.EXISTS_NOT_TAKEN 
					|| vertices[f.currentEdge]!=VertexType.NOT_TAKEN)) f.currentEdge++;
		}
		
		
		if (f.currentEdge==n) {
			if (searchStack.peek().decidedWithOre) {
				msg.add("[ERROR] ORE decision should not have been revoked!\n");
			}
			searchStack.pop();
			if (searchStack.empty()) {
				vertices[0]=VertexType.NOT_TAKEN;
			}else {
				adjMatrix[searchStack.peek().currentVertex][searchStack.peek().currentEdge]=EdgeType.EXISTS_NOT_TAKEN;
				adjMatrix[searchStack.peek().currentEdge][searchStack.peek().currentVertex]=EdgeType.EXISTS_NOT_TAKEN;
				vertices[searchStack.peek().currentEdge]=VertexType.NOT_TAKEN;
				vertices[searchStack.peek().currentVertex]=VertexType.TAKEN;
				searchStack.peek().restore(adjMatrix);
			}

		}
		else {
			adjMatrix[f.currentVertex][f.currentEdge]=EdgeType.EXISTS_TAKEN;
			adjMatrix[f.currentEdge][f.currentVertex]=EdgeType.EXISTS_TAKEN;
			vertices[f.currentEdge]=VertexType.TAKEN;
			if (f.len>=1) {
				vertices[f.currentVertex]=VertexType.TAKEN_IGNORE;
				for (int i=0;i<n;i++) {
					if (adjMatrix[f.currentVertex][i]==EdgeType.EXISTS_NOT_TAKEN) {
						System.out.println("HERE");
						searchStack.peek().addRestore(f.currentVertex, i, adjMatrix[f.currentVertex][i]);
						adjMatrix[f.currentVertex][i]=EdgeType.EXISTS_IGNORE;
						searchStack.peek().addRestore(i, f.currentVertex, adjMatrix[i][f.currentVertex]);
						adjMatrix[i][f.currentVertex]=EdgeType.EXISTS_IGNORE;
					}
				}
			}
			searchStack.push(new StackFrame(f.currentEdge,f.currentVertex, -1,f.len+1,false));

		}
	}
	
	private boolean exactOreCheck() {
		int[] vertDeg=new int[n];
		for (int i=0;i<n;i++) {
			for (int j=0;j<n;j++) {
				if (adjMatrix[i][j]==EdgeType.EXISTS_TAKEN || adjMatrix[i][j]==EdgeType.EXISTS_NOT_TAKEN) vertDeg[i]++;
			}
		}
		
		int degCnt=0;
		for (int i=0;i<n;i++) {
			if (vertices[i]==VertexType.TAKEN || vertices[i]==VertexType.NOT_TAKEN) {
				degCnt++;
			}
		}
		
		for (int i=0;i<n;i++) {
			if (vertices[i]==VertexType.TAKEN_IGNORE) continue;
			for (int j=0;j<n;j++) {
				if (vertices[j]==VertexType.TAKEN_IGNORE) continue;
				
				if (adjMatrix[i][j]==EdgeType.EXISTS_NOT_TAKEN || adjMatrix[i][j]==EdgeType.EXISTS_TAKEN) {
					//adjecent
				}
				else {
					//not adjecent
					if (vertDeg[i]+vertDeg[j]<degCnt) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	private int tryAndDoOre(int currentVertex, int originVertex) {
		
		ArrayList<Integer> toTest=new ArrayList<Integer>();
		ArrayList<Integer> toDisable=new ArrayList<Integer>();
		for (int i=0;i<n;i++) {
			if (adjMatrix[currentVertex][i]==EdgeType.DOESNT_EXIST) continue;
			if (adjMatrix[currentVertex][i]==EdgeType.EXISTS_IGNORE) continue;
			if (adjMatrix[currentVertex][i]==EdgeType.EXISTS_TAKEN) continue;
			
			if (vertices[i]==VertexType.TAKEN || vertices[i]==VertexType.TAKEN_IGNORE) {
				toDisable.add(i);
			}
			else {
				toTest.add(i);
			}
		}
		
		
		
		for (int i:toTest) {
				adjMatrix[currentVertex][i]=EdgeType.EXISTS_IGNORE;
				adjMatrix[i][currentVertex]=EdgeType.EXISTS_IGNORE;
		}
		
		for (int i:toDisable) {
			adjMatrix[currentVertex][i]=EdgeType.EXISTS_IGNORE;
			adjMatrix[i][currentVertex]=EdgeType.EXISTS_IGNORE;
		}
	
		
		for (Integer i:toTest) {
			adjMatrix[currentVertex][i]=EdgeType.EXISTS_TAKEN;
			adjMatrix[i][currentVertex]=EdgeType.EXISTS_TAKEN;
			vertices[currentVertex]=VertexType.TAKEN_IGNORE;
			vertices[i]=VertexType.TAKEN;
			
			if (adjMatrix[i][0]==EdgeType.EXISTS_NOT_TAKEN) {
				adjMatrix[i][0]=EdgeType.EXISTS_IGNORE;
				adjMatrix[0][i]=EdgeType.EXISTS_IGNORE;
				boolean res=exactOreCheck();
				adjMatrix[i][0]=EdgeType.EXISTS_NOT_TAKEN;
				adjMatrix[0][i]=EdgeType.EXISTS_NOT_TAKEN;
				if(res) return i;
			}
			else {
				if(exactOreCheck()) return i;
			}

			
			vertices[currentVertex]=VertexType.TAKEN;
			vertices[i]=VertexType.NOT_TAKEN;
			adjMatrix[currentVertex][i]=EdgeType.EXISTS_IGNORE;
			adjMatrix[i][currentVertex]=EdgeType.EXISTS_IGNORE;
		}
		
		for (int i:toTest) {
			adjMatrix[currentVertex][i]=EdgeType.EXISTS_NOT_TAKEN;
			adjMatrix[i][currentVertex]=EdgeType.EXISTS_NOT_TAKEN;
		}
		
		for (int i:toDisable) {
			adjMatrix[currentVertex][i]=EdgeType.EXISTS_NOT_TAKEN;
			adjMatrix[i][currentVertex]=EdgeType.EXISTS_NOT_TAKEN;
		}
		
		return -1;
	}

	@Override
	public EdgeType[][] getGraph() {
		return adjMatrix;
	}

	@Override
	public VertexType[] getVertices() {
		return vertices;
	}
	
	
	
}
