package methods;

import graphs.EdgeType;
import graphs.VertexType;
import methods.BruteforceCHVATAL.StackFrame;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Stack;

import grafika.Constants;
import grafika.RuntimeContainer;

public class Bruteforce extends Method {
	
	static class VertexRestore{
		int i;
		VertexType restoreType;
		public int getI() {
			return i;
		}
		public VertexType getRestoreType() {
			return restoreType;
		}
		public VertexRestore(int i, VertexType restoreType) {
			super();
			this.i = i;
			this.restoreType = restoreType;
		}
	}
	
	static class EdgeRestore{
		int i,j;
		EdgeType restoreType;
		public int getI() {
			return i;
		}
		public EdgeType getRestoreType() {
			return restoreType;
		}
		public EdgeRestore(int i, int j, EdgeType restoreType) {
			super();
			this.i = i;
			this.j = j;
			this.restoreType = restoreType;
		}
	}
	
	static class StackFrame{
		int currentEdge;
		int currentVertex;
		int originVertex;
		int len;
		boolean decidedWithOre;
		private ArrayList<EdgeRestore> er;
		
		public void restore(EdgeType[][] adjMatrix) {
			for (EdgeRestore r:er) {
				adjMatrix[r.i][r.j]=r.restoreType;
			}
		}
		
		public void addRestore(int i, int j, EdgeType restoreType) {
			er.add(new EdgeRestore(i, j, restoreType));
		}
		
		public StackFrame(int currentVertex, int originVertex, int currentEdge, int len, boolean decidedWithOre) {
			super();
			this.currentEdge = currentEdge;
			this.originVertex=originVertex;
			this.currentVertex = currentVertex;
			this.len=len;
			this.er=new ArrayList<>();
			this.decidedWithOre=decidedWithOre;
		}
	}
	
	private Stack<StackFrame> searchStack = new Stack<>();
	private EdgeType[][] adjMatrix;
	private VertexType[] vertices;
	private int n;
	
	public Bruteforce(int[][] adjMatrix) {
		this.n=adjMatrix.length;
		this.adjMatrix=new EdgeType[n][n];
		this.vertices=new VertexType[n];
		for (int i=0;i<n;i++) {
			vertices[i]=VertexType.NOT_TAKEN;
			for (int j=0;j<n;j++) {
				this.adjMatrix[i][j]=adjMatrix[i][j]==1?EdgeType.EXISTS_NOT_TAKEN:EdgeType.DOESNT_EXIST;
			}
		}
		vertices[0]=VertexType.TAKEN;
		searchStack.add(new StackFrame(0, -1, -1, 0,false));
	}
	
	public void nextStep() {
		if (searchStack.empty()) {
			RuntimeContainer.sprintFinished=true;
			RuntimeContainer.sprintFound=false;
			return;
		}
		StackFrame f = searchStack.peek();
		if (f.len==n) {
			RuntimeContainer.sprintFinished=true;
			RuntimeContainer.sprintFound=true;
			return;
		}
		
		f.currentEdge++;
		if (f.len==n-1) {
			if (adjMatrix[f.currentVertex][0]==EdgeType.EXISTS_NOT_TAKEN) f.currentEdge=0;
			else {
				searchStack.pop();
				adjMatrix[searchStack.peek().currentVertex][searchStack.peek().currentEdge]=EdgeType.EXISTS_NOT_TAKEN;
				adjMatrix[searchStack.peek().currentEdge][searchStack.peek().currentVertex]=EdgeType.EXISTS_NOT_TAKEN;
				vertices[searchStack.peek().currentEdge]=VertexType.NOT_TAKEN;
				vertices[searchStack.peek().currentVertex]=VertexType.TAKEN;
				searchStack.peek().restore(adjMatrix);
				return;
			}
		}
		else {
			while (f.currentEdge<n && (adjMatrix[f.currentVertex][f.currentEdge]!=EdgeType.EXISTS_NOT_TAKEN 
					|| vertices[f.currentEdge]!=VertexType.NOT_TAKEN)) f.currentEdge++;
		}
		
		
		if (f.currentEdge==n) {
			if (searchStack.peek().decidedWithOre) {
				throw new RuntimeException("ORE decision should not have been revoked!");
			}
			searchStack.pop();
			if (searchStack.empty()) {
				vertices[0]=VertexType.NOT_TAKEN;
			}else {
				adjMatrix[searchStack.peek().currentVertex][searchStack.peek().currentEdge]=EdgeType.EXISTS_NOT_TAKEN;
				adjMatrix[searchStack.peek().currentEdge][searchStack.peek().currentVertex]=EdgeType.EXISTS_NOT_TAKEN;
				vertices[searchStack.peek().currentEdge]=VertexType.NOT_TAKEN;
				vertices[searchStack.peek().currentVertex]=VertexType.TAKEN;
				searchStack.peek().restore(adjMatrix);
			}

		}
		else {
			adjMatrix[f.currentVertex][f.currentEdge]=EdgeType.EXISTS_TAKEN;
			adjMatrix[f.currentEdge][f.currentVertex]=EdgeType.EXISTS_TAKEN;
			vertices[f.currentEdge]=VertexType.TAKEN;
			if (f.len>=1) {
				vertices[f.currentVertex]=VertexType.TAKEN_IGNORE;
				for (int i=0;i<n;i++) {
					if (adjMatrix[f.currentVertex][i]==EdgeType.EXISTS_NOT_TAKEN) {
						searchStack.peek().addRestore(f.currentVertex, i, adjMatrix[f.currentVertex][i]);
						adjMatrix[f.currentVertex][i]=EdgeType.EXISTS_IGNORE;
						searchStack.peek().addRestore(i, f.currentVertex, adjMatrix[i][f.currentVertex]);
						adjMatrix[i][f.currentVertex]=EdgeType.EXISTS_IGNORE;
					}
				}
			}
			searchStack.push(new StackFrame(f.currentEdge,f.currentVertex, -1,f.len+1,false));

		}
	}
	
	public void draw(Graphics g) {
		int[][] c = coordinates();
		
		for (int i=0;i<n;i++){
			for (int j=i+1;j<n;j++){
				if (adjMatrix[i][j]==EdgeType.EXISTS_NOT_TAKEN){
					Graphics2D g2 = (Graphics2D) g;
				    g2.setStroke(new BasicStroke(3));
					g.setColor(Color.BLACK);
					g.drawLine(c[i][0]+Constants.size/2, c[i][1]+Constants.size/2, c[j][0]+Constants.size/2, c[j][1]+Constants.size/2);
					g2.setStroke(new BasicStroke(1));
				}
				else if (adjMatrix[i][j]==EdgeType.EXISTS_TAKEN){
					Graphics2D g2 = (Graphics2D) g;
				    g2.setStroke(new BasicStroke(3));
					g.setColor(Color.RED);
					g.drawLine(c[i][0]+Constants.size/2, c[i][1]+Constants.size/2, c[j][0]+Constants.size/2, c[j][1]+Constants.size/2);
					g2.setStroke(new BasicStroke(1));
				}
				else if (adjMatrix[i][j]==EdgeType.EXISTS_IGNORE){
					g.setColor(Color.GRAY);
					g.drawLine(c[i][0]+Constants.size/2, c[i][1]+Constants.size/2, c[j][0]+Constants.size/2, c[j][1]+Constants.size/2);
				}
			}
		}
		
		for (int i=0;i<n;i++){
			if (vertices[i]==VertexType.TAKEN){
				g.setColor(Color.RED);
			}
			else if (vertices[i]==VertexType.TAKEN_IGNORE){
				g.setColor(Color.GRAY);
			}
			else{
				g.setColor(Color.WHITE);
			}
			
			g.fillOval(c[i][0], c[i][1], Constants.size, Constants.size);
			g.setColor(Color.BLACK);
			g.drawString(Integer.toString(i), c[i][0]+Constants.size/2, c[i][1]+Constants.size/2);
			g.drawOval(c[i][0], c[i][1], Constants.size, Constants.size);
		}
	}
	
	private int[][] coordinates(){
		int[][] res = new int[n][2];
		double angle=0;
		for (int i=0;i<n;i++){
			res[i][0]=(int) (Constants.centerX+Math.cos(angle)*Constants.len);
			res[i][1]=(int) (Constants.centerY+Math.sin(angle)*Constants.len);
			angle+=2*Math.PI/n;
		}
		return res;
	}
	
	@Override
	public EdgeType[][] getGraph() {
		return adjMatrix;
	}

	@Override
	public VertexType[] getVertices() {
		return vertices;
	}
	
}
