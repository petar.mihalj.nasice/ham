package methods;

import java.awt.Graphics;
import java.util.ArrayList;

import graphs.EdgeType;
import graphs.VertexType;

public abstract class Method {
	public abstract void nextStep();
	public abstract EdgeType[][] getGraph();
	public abstract VertexType[] getVertices();
	
	public ArrayList<String> msg;
	
	public Method() {
		this.msg=new ArrayList<>();
	}
	
}
